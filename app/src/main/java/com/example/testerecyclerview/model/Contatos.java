package com.example.testerecyclerview.model;

public class Contatos {
    private String nomeContact;
    private String dataContact;
    private String telContact;

    public Contatos() {}

    public Contatos(String nomeContact, String dataContact, String telContact) {
        this.nomeContact = nomeContact;
        this.dataContact = dataContact;
        this.telContact = telContact;
    }

    public String getNomeContact() {
        return nomeContact;
    }

    public void setNomeContact(String nomeContact) {
        this.nomeContact = nomeContact;
    }

    public String getDataContact() {
        return dataContact;
    }

    public void setDataContact(String dataContact) {
        this.dataContact = dataContact;
    }

    public String getTelContact() {
        return telContact;
    }

    public void setTelContact(String telContact) {
        this.telContact = telContact;
    }
}
