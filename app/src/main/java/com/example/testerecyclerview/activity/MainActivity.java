package com.example.testerecyclerview.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.testerecyclerview.R;
import com.example.testerecyclerview.adapter.AgendAdapter;
import com.example.testerecyclerview.model.Contatos;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Contatos> contact = new ArrayList<>();
     RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);

        this.listaContatos();
        AgendAdapter adapter = new AgendAdapter(contact);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

    }

    public void listaContatos(){
        Contatos agenda = new Contatos("Maria","11930812120","25/06/2020");
        contact.add(agenda);
        agenda =  new Contatos("Natã","11933207837","25/04/2021");
        contact.add(agenda);
        agenda =  new Contatos("Samuel","11955207437","25/04/2021");
        contact.add(agenda);
        agenda =  new Contatos("Natã","11988217837","25/04/2021");
        contact.add(agenda);
        agenda =  new Contatos("Natã","11944207886","25/04/2021");
        contact.add(agenda);
        agenda =  new Contatos("Natã","11933207837","25/04/2021");
        contact.add(agenda);
        agenda =  new Contatos("Natã","11933207837","25/04/2021");
        contact.add(agenda);
        agenda =  new Contatos("Natã","11933207837","25/04/2021");
        contact.add(agenda);
    }

}