package com.example.testerecyclerview.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testerecyclerview.R;
import com.example.testerecyclerview.model.Contatos;

import java.util.List;

public class AgendAdapter extends RecyclerView.Adapter<AgendAdapter.MyViewHolder> {
    List<Contatos> contatos;

    public AgendAdapter(List<Contatos> contatos) {
        this.contatos = contatos;
    }

    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLista = LayoutInflater.from(parent.getContext()).inflate(R.layout.agenda_adapter,parent,false);
        return new MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Contatos dataSet = contatos.get(position);
        holder.nome.setText(dataSet.getNomeContact());
        holder.data.setText(dataSet.getDataContact());
        holder.telefone.setText(dataSet.getDataContact());
    }

    @Override
    public int getItemCount() {

        return contatos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
             TextView nome;
             TextView data;
             TextView telefone;

        public MyViewHolder(View itemView) {
            super(itemView);
            nome = itemView.findViewById(R.id.textNome);
            data = itemView.findViewById(R.id.textData);
            telefone = itemView.findViewById(R.id.textTelefone);
        }
    }
}
